package testLeaf;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Tc002_EditLead  extends ProjectSpecificMethod{
	
	@BeforeTest
	public void setData() {
		testcaseName="Tc002_EditLead";
		testdesc="Edit Company";
		author="Suresh";
		category="Smoke";
	}	

	//@Test(groups = "sanity" , dependsOnGroups= {"smoke"}, alwaysRun = true)
	@Test(dataProvider="fetchData")
	public void login(String ftname, String cpname) throws InterruptedException  {
		


/*		startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement uname = locateElement("id", "username");type(uname, "DemoSalesManager");

		WebElement pwd = locateElement("id", "password");type(pwd, "crmsfa");

		WebElement button = locateElement("class", "decorativeSubmit"); click(button);

		WebElement crmlink = locateElement("linktext", "CRM/SFA");click(crmlink);

		//Thread.sleep(3000);
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		wait1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.linkText("Leads")));*/
		WebElement leadlink = locateElement("linktext", "Leads");click(leadlink);

		WebElement findlead = locateElement("xpath", "//a[text()='Find Leads']");click(findlead);

		WebElement fname = locateElement("xpath", "((//input[@name='firstName'])[3])");type(fname, ftname);

		WebElement findleadbutton = locateElement("xpath", "//button[text()='Find Leads']");click(findleadbutton);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement clickable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[@class='linktext'])[6]")));
		
		//Thread.sleep(1000);
		WebElement clickL = locateElement("xpath", "(//a[@class='linktext'])[6]");click(clickL);

		boolean title = verifyTitle("View Lead | opentaps CRM");

		if (title) {
			WebElement edit = locateElement("linktext", "Edit"); click(edit);

			locateElement("id", "updateLeadForm_companyName").clear();

			WebElement compname = locateElement("id", "updateLeadForm_companyName");type(compname, cpname);

			WebElement submit = locateElement("xpath", "//input[@name='submitButton']");click(submit);

			WebElement comptext = locateElement("id", "viewLead_companyName_sp");
			String companyname = getText(comptext);

			if (companyname.contains("TCS")) {

				System.out.println("Company name has beend updated");
			} else {
				System.out.println("Company name has not beend updated");

			}

		} else {

			System.out.println("test case failed");

		}

	}
	@DataProvider(name="fetchData")
	public String[][] data() throws IOException {
		
/*		String[][] data = new String[2][3];
		data[0][0] = "TCS";
		data[0][1] = "userdummy";
		data[0][2] = "123456";
		
		data[1][0]="CTS";
		data[1][1]="userdummy1";
		data[1][2]="1234567";
		return data;*/
		String[][] getdata = ReadExcel.readexcel("editlead");
		return getdata;
	}
}
