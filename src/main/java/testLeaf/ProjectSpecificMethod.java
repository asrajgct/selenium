package testLeaf;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;



public class ProjectSpecificMethod extends SeMethods {

	@Parameters({"browser","url","uname","pwd"})
	//@BeforeMethod(groups = "common")
	@BeforeMethod()
	public void login1(String browser, String url,String uname, String pwd) {
		
	startApp( browser, url);

	WebElement eleUsername = locateElement("id", "username");type(eleUsername, uname);

	WebElement elePassword = locateElement("id", "password");type(elePassword, pwd);

	WebElement eleLogin = locateElement("class", "decorativeSubmit");click(eleLogin);

	WebElement crmlink = locateElement("linktext", "CRM/SFA");click(crmlink);

	WebElement leads = locateElement("linktext", "Leads");	click(leads);
	}
	
	//@AfterMethod(groups = "common")
	@AfterMethod
	public void logout() {
		closeBrowser();
	}
	
}
