package testLeaf;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import testLeaf.ReadExcel;

public class Tc003_DuplicateLead extends ProjectSpecificMethod {
	
	@BeforeTest
	public void setData() {
		testcaseName="Tc003_DuplicateLead";
		testdesc="Duplicate Lead";
		author="Suresh";
		category="Smoke";
	}	

	//@Test(priority = 1)

	//@Test(enabled = false)
	
	@Test(dataProvider="fetchData")
	public void login( String emailid) {

		/*startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement eleUsername = locateElement("id", "username");type(eleUsername, "DemoSalesManager");

		WebElement elePassword = locateElement("id", "password");type(elePassword, "crmsfa");

		WebElement eleLogin = locateElement("class", "decorativeSubmit");click(eleLogin);

		WebElement crmlink = locateElement("linktext", "CRM/SFA");click(crmlink);

		WebElement leadslink = locateElement("linktext", "Leads");click(leadslink);*/

		WebElement findlead = locateElement("xpath", "//a[text()='Find Leads']");click(findlead);

		WebElement emailtab = locateElement("xpath", "//span[text()='Email']");click(emailtab);

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='emailAddress']")));

		WebElement email = locateElement("xpath", "//input[@name='emailAddress']");type(email, emailid);

		WebElement findbutton = locateElement("xpath", "//button[text()='Find Leads']");click(findbutton);

		WebDriverWait wait1 = new WebDriverWait(driver, 50);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[@class='linktext'])[7]")));

		WebElement text0 = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String uid = getText(text0);

		WebElement text = locateElement("xpath", "(//a[@class='linktext'])[6]");
		String fname = getText(text);

		WebElement text1 = locateElement("xpath", "(//a[@class='linktext'])[7]");
		String lname = getText(text1);

		click(text);

		WebElement duplicatebutton = locateElement("linktext", "Duplicate Lead");click(duplicatebutton);

		boolean title = verifyTitle("Duplicate Lead | opentaps CRM");



		if (title) {

			WebElement createlead = locateElement("xpath", "//input[@name='submitButton']");
			click(createlead);

			WebElement text2 = locateElement("xpath", "//span[@id='viewLead_firstName_sp']");
			String ufname = getText(text2);

			WebElement text3 = locateElement("xpath", "//span[@id='viewLead_lastName_sp']");
			String ulname = getText(text3);

			WebElement text4 = locateElement("xpath", "//span[@id='viewLead_companyName_sp']");
			String ulid = getText(text4);

			if ((fname.equalsIgnoreCase(ufname) && (lname.equalsIgnoreCase(ulname) && (uid != ulid)))) {

				System.out.println("Duplicate lead has been created and the id is " +ulid );

			} else 
			{
				System.out.println("Duplicate ID has not been created");
			}

		} else 
		{
			System.out.println("Oops! wrong page");
		}
	}
	
	@DataProvider(name="fetchData")
	public String[][] data() throws IOException {
		
/*		String[][] data = new String[2][3];
		data[0][0] = "TCS";
		data[0][1] = "userdummy";
		data[0][2] = "123456";
		
		data[1][0]="CTS";
		data[1][1]="userdummy1";
		data[1][2]="1234567";
		return data;*/
		String[][] getdata = ReadExcel.readexcel("duplicatelead");
		return getdata;
	}
}
