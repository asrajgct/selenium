package testLeaf;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Tc001CreateLead extends ProjectSpecificMethod{

	
	
	@BeforeTest
	public void setData() {
		testcaseName="Tc001CreateLead";
		testdesc="Create a new Lead";
		author="Suresh";
		category="Smoke";
	}	
	//@Test(enabled= false)
	//@Test(invocationCount = 2)
	
	//@Test(groups = "smoke")
	@Test(dataProvider="fetchData")
	public void login(String cname, String fname, String lname) {
	/*startApp("chrome", "http://leaftaps.com/opentaps");
	
	WebElement eleUsername = locateElement("id", "username");
	type(eleUsername, "DemoSalesManager");
	
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, "crmsfa");
	
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	
	WebElement crmlink = locateElement("linktext", "CRM/SFA");
	click(crmlink);
	
	WebElement leads = locateElement("linktext", "Leads");
	click(leads);*/
	
	WebElement createLead = locateElement("linktext", "Create Lead");
	click(createLead);
	
	WebElement comp = locateElement("id", "createLeadForm_companyName");
	type(comp, cname);
	
	WebElement Fname = locateElement("id", "createLeadForm_firstName");
	type(Fname, fname);
	
	WebElement Lname= locateElement("id", "createLeadForm_lastName");
	type(Lname, lname);

	WebElement FLname = locateElement("id", "createLeadForm_firstNameLocal");
	type(FLname, "sampuser");

	WebElement LLname= locateElement("id", "createLeadForm_lastNameLocal");
	type(LLname, "123");

	WebElement Ptitle= locateElement("id", "createLeadForm_personalTitle");
	type(Ptitle, "Mr");

	WebElement Gtitle= locateElement("id", "createLeadForm_generalProfTitle");
	type(Gtitle, "SampleTitle");

	WebElement Depname = locateElement("id", "createLeadForm_departmentName");
	type(Depname, "IT");

	WebElement Revenue= locateElement("id", "createLeadForm_annualRevenue");
	type(Revenue, "500000");

	WebElement Noemployee= locateElement("id", "createLeadForm_numberEmployees");
	type(Noemployee, "3000000");

	WebElement siccode= locateElement("id", "createLeadForm_sicCode");
	type(siccode, "123456");

	WebElement ticker= locateElement("id", "createLeadForm_tickerSymbol");
	type(ticker, "symbol");

	WebElement Desc= locateElement("id", "createLeadForm_description");
	type(Desc, "test lead creation");

	WebElement Impnote= locateElement("id", "createLeadForm_importantNote");
	type(Impnote, "Mandatory fields must be filled");

	WebElement CountryCode= locateElement("id", "createLeadForm_primaryPhoneCountryCode");
	type(CountryCode, "100");

	WebElement Phonecode= locateElement("id", "createLeadForm_primaryPhoneAreaCode");
	type(Phonecode, "10");

	WebElement Phoneno= locateElement("id", "createLeadForm_primaryPhoneNumber");
	type(Phoneno, "7897987981");

	WebElement PhoneExt= locateElement("id", "createLeadForm_primaryPhoneExtension");
	type(PhoneExt, "123465");

	WebElement phoneask= locateElement("id", "createLeadForm_primaryPhoneAskForName");
	type(phoneask, "dummy user");

	WebElement Pemail= locateElement("id", "createLeadForm_primaryEmail");
	type(Pemail, "sample1234@gmail.com");

	WebElement Weburl= locateElement("id", "createLeadForm_primaryWebUrl");
	type(Weburl, "www.google.com");

	WebElement Gname= locateElement("id", "createLeadForm_generalToName");
	type(Gname, "sampleToName");

	WebElement GAname= locateElement("id", "createLeadForm_generalAttnName");
	type(GAname, "sampleattentionname");

	WebElement Gaddress1= locateElement("id", "createLeadForm_generalAddress1");
	type(Gaddress1, "sampleaddress1");

	WebElement Gaddress2= locateElement("id", "createLeadForm_generalAddress2");
	type(Gaddress2, "sampleaddress2");

	WebElement GCity= locateElement("id", "createLeadForm_generalCity");
	type(GCity, "samplecity");

	WebElement GPcode= locateElement("id", "createLeadForm_generalPostalCode");
	type(GPcode, "123456");

	WebElement GPcodeext= locateElement("id", "createLeadForm_generalPostalCodeExt");
	type(GPcodeext, "044");

	//Drop down
	
	WebElement datasourceid= locateElement("id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(datasourceid, "Existing Customer");
	
	WebElement campaignid= locateElement("id", "createLeadForm_marketingCampaignId");
	selectDropDownUsingValue(campaignid, "CATRQ_CARNDRIVER");
	
	WebElement industryid= locateElement("id", "createLeadForm_industryEnumId");
	selectDropDownUsingValue(industryid, "IND_SOFTWARE");
	
	//	Remaining
	WebElement Currency= locateElement("id", "createLeadForm_currencyUomId");
	type(Currency, "INR - Indian Rupee");

	WebElement ownership = locateElement("id", "createLeadForm_ownershipEnumId");
	type(ownership, "Corporation");

	WebElement Countrycode= locateElement("id", "createLeadForm_generalCountryGeoId");
	type(Countrycode, "India");

	WebElement StateId= locateElement("id", "createLeadForm_generalStateProvinceGeoId");
	type(StateId, "TAMILNADU");

	WebElement Bdate= locateElement("id", "createLeadForm_birthDate");
	type(Bdate, "12/12/12");

	WebElement Submitbutton= locateElement("class", "smallSubmit");
	Submitbutton.click();
	}
	@DataProvider(name="fetchData")
	public String[][] data() throws IOException {
		
/*		String[][] data = new String[2][3];
		data[0][0] = "TCS";
		data[0][1] = "userdummy";
		data[0][2] = "123456";
		
		data[1][0]="CTS";
		data[1][1]="userdummy1";
		data[1][2]="1234567";
		return data;*/
		String[][] getdata = ReadExcel.readexcel("createlead");
		return getdata;
	}
	
	}
	








