package testLeaf;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	public static String[][] readexcel(String fileName) throws IOException {
		
		
		XSSFWorkbook file = new XSSFWorkbook("./drivers/"+fileName+".xlsx");
		
		XSSFSheet sheet = file.getSheetAt(0);
		
		int rowcount = sheet.getLastRowNum();
		
		System.out.println(rowcount);
		
		int cell = sheet.getRow(0).getLastCellNum();
		
		System.out.println(cell);
		
		String[][] data = new String[rowcount][cell];
		
		for (int i = 1; i <= rowcount; i++) {
			
			XSSFRow row = sheet.getRow(i);
			
			for (int j = 0; j < cell; j++) {
				
				XSSFCell celldata = row.getCell(j);
				
				data[i-1][j] = celldata.getStringCellValue();
			}
		}
		return data;
	}

}
