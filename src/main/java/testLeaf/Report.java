package testLeaf;


import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class Report {
	public static ExtentReports extent;
	public static ExtentHtmlReporter html;
	public ExtentTest test;
	public String testcaseName, testdesc, author, category;
	
	@BeforeSuite
	public void beforeSuite() {
		html = new ExtentHtmlReporter("./drivers/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		//attach report
		extent.attachReporter(html);

	}
	
	@BeforeClass
	public void beforeClass() {
		test = extent.createTest(testcaseName, testdesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	//report step
	public void reportStep(String status, String data) {
		if (status.equalsIgnoreCase("Pass")) {
			System.out.println(status);
			System.out.println(data);
			test.pass(data);
		} else if (status.equalsIgnoreCase("Fail")) {
			test.fail(data);
		}else if (status.equalsIgnoreCase("Warning")) {
			test.warning(data);
		}
		
	}

	@AfterSuite
	public void afterSuite() {
		extent.flush();
	}

}
