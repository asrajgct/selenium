package testLeaf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;


public class SeMethods extends Report implements WdMethods {

	RemoteWebDriver driver = null;
	//ChromeDriver driver = null; 

	//Actions actn = new Actions(driver);
	@Override
	public void startApp(String browser, String url) {

		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}

			driver.manage().window().maximize();
			driver.get(url);
			//System.out.println("The browser "+browser+" launched successfully");
			 reportStep("Pass", "The browser "+browser+" launched successfully");

		} catch (WebDriverException e) {
			System.err.println("WebDriveException " +e.getMessage());
			reportStep("Fail", "The browser "+browser+" launch Failed");
		}
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue); 
			case "class": return  driver.findElementByClassName(locValue);
			case "linktext" : return driver.findElementByLinkText(locValue);
			case "Partiallinktext" : return driver.findElementByPartialLinkText(locValue);
			case "xpath": return driver.findElementByXPath(locValue); 
			case "name": return  driver.findElementByClassName(locValue);
			case "Css" : return driver.findElementByCssSelector(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element not found");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException " + e.getMessage());
		}
		takeSnap();
		return null;
	}

	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementsById(locValue); 
			case "class": return  driver.findElementsByClassName(locValue);
			case "linktext" : return driver.findElementsByLinkText(locValue);
			case "Partiallinktext" : return driver.findElementsByPartialLinkText(locValue);
			case "xpath": return driver.findElementsByXPath(locValue); 
			case "name": return  driver.findElementsByClassName(locValue);
			case "Css" : return driver.findElementsByCssSelector(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element not found");
		}catch (WebDriverException e) {
			System.err.println("WebDriver Excception" + e.getMessage());
		}
		takeSnap();
		return null;
	}


	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);	
			//System.out.println("The data "+data+" entered successfully");
			reportStep("Pass", "The data "+data+" entered successfully");
		} catch (WebDriverException e) {
			reportStep("Fail", "The data "+data+" did not entered" +e.getMessage());
		}
		takeSnap();
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			//System.out.println("The element "+ele+ "click successfully");
			reportStep("Pass", "The element "+ele+ "click successfully");
		} catch (WebDriverException e) {
			reportStep("Fail", "The data "+ele+" did not entered" +e.getMessage());
		}
		//takeSnap();
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub

		String text = ele.getText();

		return text;
	}

	/*	public void Title() {
		String pagetitle = driver.getTitle();
		System.out.println("Page Title is "+pagetitle);
	}*/

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select sel = new Select(ele);
			sel.selectByVisibleText(value);
			//System.out.println("The value "+value+" entered successfully");
			reportStep("Pass", "The value "+value+" entered successfully");
			
		} catch (WebDriverException e) {
			reportStep("Fail", "The value "+value+" did not entered "+ e.getMessage());
		}
		takeSnap();
	}



	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}

	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select sel = new Select(ele);
			sel.selectByValue(value);
			//System.out.println("The value "+value+" entered successfully");
			reportStep("Pass", "The value "+value+" entered successfully");
		} catch (WebDriverException e) {
			reportStep("Fail", "The value "+value+" didnot entered");
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		String pagetitle = driver.getTitle();
		if (pagetitle.equalsIgnoreCase(expectedTitle)) {

			System.out.println("Page Title is "+pagetitle);
			return true;
		} else {
			return false;
		}

	}

	/*	public boolean verifyText(String expectedtext) {
		// TODO Auto-generated method stub
		String companytitle = driver.getTitle();

		if (companytitle.contains(expectedtext)){

			System.out.println("Page Title is "+companytitle);
			return true;
		} else {
			return false;
		}

	}*/

	public int listsize(WebElement ele) {

		List<WebElement> list1 = new ArrayList<WebElement>();
		list1.add(ele);
		return list1.size();
	}

	public List<WebElement> listelements(WebElement ele) {

		List<WebElement> list2 = new ArrayList<WebElement>();
		list2.add(ele);
		return list2;
	}

	public List<String> liststring(String ele){

		List<String> list3 = new ArrayList<String>();
		list3.add(ele);
		return list3;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./drivers/img.png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			System.err.println("IO exception occured");
		}

	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub

	}

	@Override
	public Point getLocation(WebElement ele) {
		// TODO Auto-generated method stub

		return getLocation(ele);
	}

}
