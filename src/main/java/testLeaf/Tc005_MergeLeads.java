package testLeaf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Tc005_MergeLeads extends ProjectSpecificMethod{

	
	@BeforeTest
	public void setData() {
		testcaseName="Tc005_MergeLeads";
		testdesc="Create a new Lead";
		author="Suresh";
		category="Smoke";
	}
	
	@Test(dataProvider="fetchData")
	public void Edit(String ftname1,String ftname2) throws InterruptedException {

		/*startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement eleUsername = locateElement("id", "username");type(eleUsername, "DemoSalesManager");

		WebElement elePassword = locateElement("id", "password");type(elePassword, "crmsfa");

		WebElement eleLogin = locateElement("class", "decorativeSubmit");click(eleLogin);

		WebElement crmlink = locateElement("linktext", "CRM/SFA");click(crmlink);

		WebElement leads = locateElement("linktext", "Leads");click(leads);*/

		WebElement merge = locateElement("linktext", "Merge Leads");click(merge);

		WebElement icon1 = locateElement("xpath", "((//img[@alt='Lookup'])[1])");click(icon1);

		Set<String> windowHandles = driver.getWindowHandles();
		List<String> refList = new ArrayList<String>();
		refList.addAll(windowHandles);

		driver.switchTo().window(refList.get(1));
		driver.manage().window().maximize();

		WebElement fname = locateElement("xpath", "(//input[@name='firstName'])");type(fname, ftname1);

		WebElement findbutton = locateElement("xpath", "//button[text()='Find Leads']");click(findbutton);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement clickable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//a[@class='linktext'])[6])")));


		WebElement id = locateElement("xpath", "((//a[@class='linktext'])[1])");
		String uid = getText(id);

		WebElement idclick = locateElement("xpath", "((//a[@class='linktext'])[1])");click(idclick);

		driver.switchTo().window(refList.get(0));

		WebElement icon2 = locateElement("xpath", "((//img[@alt='Lookup'])[2])");click(icon2);

		Set<String> windowHandles1 = driver.getWindowHandles();
		List<String> refList1 = new ArrayList<String>();
		refList1.addAll(windowHandles1);

		driver.switchTo().window(refList1.get(1));
		driver.manage().window().maximize();

		WebElement fname1 = locateElement("xpath", "((//input[@name='firstName'])[1])");type(fname1, ftname2);

		Thread.sleep(1000);
		WebElement findbutton1 = locateElement("xpath", "//button[text()='Find Leads']");click(findbutton1);

		Thread.sleep(1000);
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		WebElement clickable1 = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//a[@class='linktext'])[7])")));

		WebElement user2 = locateElement("xpath", "((//a[@class='linktext'])[7])");click(user2);
		
		driver.switchTo().window(refList.get(0));

		WebElement meargelead = locateElement("xpath", "//a[text()='Merge']");click(meargelead);

		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().accept();

		WebElement search = locateElement("xpath", "//a[text()='Find Leads']");click(search);

		WebElement sendid = locateElement("xpath", "//input[@name='id']");type(sendid, uid);

		WebElement findbutton2 = locateElement("xpath", "//button[text()='Find Leads']");click(findbutton2);

		WebDriverWait wait2 = new WebDriverWait(driver, 60);
		WebElement clickable2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='No records to display']")));

		WebElement text = locateElement("xpath", "//div[text()='No records to display']");
		String message = getText(text);

		if (message.equalsIgnoreCase("No records to display")) {
			System.out.println("Lead Details Has been merged,Test Case is Passed");

		} else {
			System.out.println("Lead details has not been merged,Test Case is failed");
		}

	}

	@DataProvider(name="fetchData")
	public String[][] data() throws IOException {
		
/*		String[][] data = new String[2][3];
		data[0][0] = "TCS";
		data[0][1] = "userdummy";
		data[0][2] = "123456";
		
		data[1][0]="CTS";
		data[1][1]="userdummy1";
		data[1][2]="1234567";
		return data;*/
		String[][] getdata = ReadExcel.readexcel("Mergelead");
		return getdata;
	}

}


