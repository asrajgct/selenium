package testLeaf;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Tc004Deletelead extends ProjectSpecificMethod {
	
	@BeforeTest
	public void setData() {
		testcaseName="Tc004DeleteLead";
		testdesc="Delete Lead";
		author="Suresh";
		category="Smoke";
	}	

	//@Test(groups = "regression" , dependsOnGroups= {"smoke"}, alwaysRun = true)
	@Test(dataProvider="fetchData")
	public void login(String phno) throws InterruptedException {
		/*startApp("chrome", "http://leaftaps.com/opentaps");

		WebElement eleUsername = locateElement("id", "username");type(eleUsername, "DemoSalesManager");

		WebElement elePassword = locateElement("id", "password");type(elePassword, "crmsfa");

		WebElement eleLogin = locateElement("class", "decorativeSubmit");click(eleLogin);

		WebElement crmlink = locateElement("linktext", "CRM/SFA");click(crmlink);

		WebElement leadslink = locateElement("linktext", "Leads");click(leadslink);*/

		WebElement findlead = driver.findElementByXPath("//a[text()='Find Leads']");click(findlead);

		WebElement phnumbertab = locateElement("xpath", "//span[text()='Phone']");click(phnumbertab);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='phoneNumber']")));

		WebElement phnumber = locateElement("xpath", "//input[@name='phoneNumber']");type(phnumber, phno);

		WebElement findbutton = locateElement("xpath", "//button[text()='Find Leads']");click(findbutton);

		Thread.sleep(5000);
		
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[@class='linktext'])[4]")));
		
		WebElement text = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String uid = getText(text);
		click(text);

		WebDriverWait wait2 = new WebDriverWait(driver, 30);
		wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Delete']")));
		
		WebElement deletebutton = locateElement("xpath", "//a[text()='Delete']");click(deletebutton);

		WebElement findledbutton = locateElement("xpath", "//a[text()='Find Leads']");click(findledbutton);

		WebElement sendid = locateElement("xpath", "//input[@name='id']");type(sendid, uid);

		WebElement findagain = locateElement("xpath", "//button[text()='Find Leads']");click(findagain);
		
		WebDriverWait wait3 = new WebDriverWait(driver, 30);
		wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='x-paging-info']")));

		WebElement text2 = locateElement("xpath", "//div[@class='x-paging-info']");
		String errortext = getText(text2);

		Thread.sleep(3000);
		if (errortext.equalsIgnoreCase("No records to display")) 
		{
			System.out.println("User has been deleted successfully");
		} else 
		{
			System.out.println("User Id deletion failed");
		} 
		
	}

	@DataProvider(name="fetchData")
	public String[][] data() throws IOException {
		
/*		String[][] data = new String[2][3];
		data[0][0] = "TCS";
		data[0][1] = "userdummy";
		data[0][2] = "123456";
		
		data[1][0]="CTS";
		data[1][1]="userdummy1";
		data[1][2]="1234567";
		return data;*/
		String[][] getdata = ReadExcel.readexcel("deletelead");
		return getdata;
	}
}
